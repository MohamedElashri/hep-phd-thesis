# PhD thesis template for high energy physics

Contents:
1. [Introduction](#introduction)
2. [A simple example](#myExample)
3. [Andy's CTAN example](#andysExample)

## Introduction<a name="introduction"></a>
Here you will find two PhD thesis templates based on the hepthesis class for LaTeX. The hepthesis class written by Andy Buckley to assist in writting PhD or master theses in High Energy Physics (HEP). Full documentation can be found [here](https://ctan.org/tex-archive/macros/latex/contrib/hepthesis?lang=en "CTAN.org").

Firstly there is [a very simple example](#myExample) which is designed to compile easily and which I wrote based on Andy's *real* example from the CTAN webpage. Then there are [some notes](https://gitlab.com/neilwarrack/hep-phd-thesis#compiling-andys-ctan-example "Compiling Andy's CTAN example") on how to compile Andy's example from the CTAN hepthesis page.

## A simple example<a name="myExample"></a>
My very simple example should compile easily once you have cloned this repo and cd'ed into the hep-phd-thesis directory. On the command line execute the following commands:
```bash
# This example uses pdflatex on linux
export TEXINPUTS=./styles:$TEXINPUTS # This tells pdflatex where to look for added style files.
pdflatex thesis.tex # this creates the .aux file needed to create the .bib file.
bibtex thesis.aux   # this creates the .bib file needed to create the bibliography.
pdflatex thesis.tex # this creates the document with the bibliography.
pdflatex thesis.tex # this creates the correct reference numbers.
```
_That's it! You should now have a fully linked up file called thesis.pdf!_


## Andy's CTAN example<a name="andysExample"></a>

_*UPDATE*: The template now exists on Overleaf in a more imediately functional way. [Click here](https://www.overleaf.com/latex/templates/hepthesis-a-template-for-academic-reports-and-phd-theses/wqrzrsppqnmb) to visit that template._


To compile the example.tex file found in the example directory on the [hepthesis class webpage](https://ctan.org/tex-archive/macros/latex/contrib/hepthesis?lang=en "CTAN.org") you will need some extra style files (`abhepexpt.sty`, `abhep.sty` and `abmath.sty`) which can be found here, in the `styles` directory of this repository. I presume these were also written by Andy Buckley who authored the `hepthesis` class, but they are missing from the CTAN webpage for some reason... 

You can now download and unzip the `hepthesis` directory from the CTAN hepthesis page linked above and copy the `styles` directory from this repo into the `examples` directory (which is inside your newly unzipped hepthesis directory and which is where you will find all the `.tex` files, etc. which will be used to create your pdf file).

Lastly, prior to compiling, you must let your latex compiler know where these extra style files are! There is documentation for the class contained in the hepthesis.pdf file which comes with the download from the CTAN webpage.

Here is an example of how you can do all that (using `pdflatex` on the linux command line):

```bash
# clone this repo:
git clone git@gitlab.com:neilwarrack/hep-phd-thesis.git
# download the hepthesis class to get the example files:
wget -O hepthesis.zip http://mirrors.ctan.org/macros/latex/contrib/hepthesis.zip
# unzip it:
unzip hepthesis.zip
# copy the extra style files into the example directory:
cp -r hep-phd-thesis/styles hepthesis/example/
# move into the example directory:
cd hepthesis/example
# tell latex where the new style files are:
export TEXINPUTS=./styles:$TEXINPUTS
# compile your tex files:
pdflatex example.tex
```
This should produce a file called `example.pdf`!

In this example we compiled the pdf document with a LaTeX compiler called `pdflatex` which already has the `hepthesis` LaTeX class included. You may need to check your latex copiler has this class. A quick way to discover if you have the class is to search for the class file, e.g.:

```bash
find / -name 'hepthesis.cls' 2>&1 | grep -v "Permission denied"
```

_You will notice that there is no bibliography. You must create the `.bib` file yourself. Andy's documentation suggest that you use the Inspire bib compiler but the link is broken and I've made no attempt to go and figure out how this works! Instead I created a simple example with a hand written reference file (see above). In reality you can just get references from google scholar and copy and paste the bibtex code directly into a bib file and compile it using `bibtex yourbibfile.bib`, then recompiling your main latex file again... and then once more..._